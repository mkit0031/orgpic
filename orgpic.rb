if ARGV.length == 0 then
    puts "Usage: orgpic.rb [WebAppName] [Path]"
    puts "Example: orgpic.rb Twitter"
    puts "WebAppList: pixiv Twitter"
    exit
else
    app = ARGV[0]
    path = ARGV[1]
end

def getSavePath(app, path, file)
    pattern = nil
    savePath = nil

    if app == "Twitter"
        pattern = /#{path}\/([^-]+)-.+/
    elsif app == "pixiv"
        pattern = /#{path}\/([^\.]+)\.([^\.]+)\..+/
    end

    m = pattern.match(file)
    if app == "Twitter"
        savePath = m[1]
    elsif app == "pixiv"
        savePath = m[2] + "_" + m[1]
    end

    return savePath
end

# デフォルトはカレントディレクトリ
if path == nil
    path = "./"
end

dir = File.expand_path(path)

# ディレクトリを移動
Dir.chdir(dir)
# ファイル一覧を取得
files = Dir.glob(dir + "/*.{jpg,png,JPG,PNG}") 

files.each do |f|
    savePath = getSavePath(app, dir, f)
    # ディレクトリが存在しなかったら作成
    if !Dir.exist?(savePath) then
        Dir.mkdir(savePath)
    end

    # ファイルをWebAppパターンに一致するディレクトリに移動
    File.rename(f, File.dirname(f) + "/" + savePath + "/" + File.basename(f))
end


